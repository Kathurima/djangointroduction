boto3==1.9.228
botocore==1.12.228
Django==3.0.4
django-crispy-forms==1.7.2
django-storages==1.7.2
docutils==0.15.2
jmespath==0.9.4
Pillow==6.1.0
python-dateutil==2.8.0
pytz==2019.2
s3transfer==0.2.1
six==1.12.0
sqlparse==0.3.0
urllib3==1.25.3
