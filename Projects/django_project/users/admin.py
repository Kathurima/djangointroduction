from django.contrib import admin
from .models import Profile

class UsersAdmin(admin.ModelAdmin):
    """docstring for UsersAdmin."""
    list_display = ('user',)
    search_fields = ('user',)
    list_filter = ['user']

admin.site.register(Profile, UsersAdmin)
