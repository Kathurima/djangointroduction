from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

class UserRegisterForm(UserCreationForm):
	"""docstring for UserRegistrationForm"""
	email = forms.EmailField()	# Adds the form field to the django generated form in views.py

	class Meta:
		"""docstring for Meta"""
		# Gives us nested names for configuration and keeps configurations
		# in one place and in this case the model to be affected is the 'User' model
		# i.e the model in which the data will be saved.
		model = User
		fields = ['username', 'email', 'password1', 'password2']	# Shows the order of fields in the form

# Model form to update username and email
class UserUpdateForm(forms.ModelForm):
	"""docstring for UserUpdateForm"""
	email = forms.EmailField()

	class Meta:
		"""docstring for Meta"""
		model = User
		fields = ['username', 'email']

# Model form to update profile
class ProfileUpdateForm(forms.ModelForm):
	"""docstring for ProfileUpdateForm"""
	class Meta:
		"""docstring for Meta"""
		model = Profile
		fields = ['image']
