from django.contrib import admin
from contactus.models import ContactUs

# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    """docstring for CommentAdmin."""
    list_display = ('name', 'email', 'subject', 'date')
    search_fields = ('name', 'email',)
    date_hierarchy = 'date'
    list_filter = ['email']

admin.site.register(ContactUs, ContactAdmin)
