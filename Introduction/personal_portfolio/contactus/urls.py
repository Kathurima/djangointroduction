from django.conf.urls import url
from django.contrib import admin
from . import views

app_name = "contactus"
urlpatterns = [
     url('', views.contact_us_view, name='contact_us'),
]
