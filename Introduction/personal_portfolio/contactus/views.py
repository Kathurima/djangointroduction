from django.shortcuts import render
from .forms import ContactForm
from contactus.models import ContactUs
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import mail_admins
from django.contrib import messages

def contact_us_view(request):
    form = ContactForm()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            # Saving mail in the database
            msg = ContactUs(
                name = form.cleaned_data['name'],
                email = form.cleaned_data['email'],
                subject = form.cleaned_data['subject'],
                message = form.cleaned_data['message'],
            )
            msg.save()
            # Sending mail to admins
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            subject = "You have a new message from {}:{}".format(name, email)
            message = "Subject: {}\n\nMessage: {}".format(form.cleaned_data['subject'], form.cleaned_data['message'])
            mail_admins(subject, message)
            messages.add_message(request, messages.INFO, 'Mail sent.')

            return HttpResponseRedirect('')
    else:
        form = ContactForm()

    context= {'form': form}
    return render(request, 'contact_us/contact.html', context)
