from django import forms


class ContactForm(forms.Form):
    name= forms.CharField(max_length=60, widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Your name"
        })
    )
    email= forms.CharField(max_length=60, widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Your email"
        })
    )
    subject= forms.CharField(max_length=60, widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Subject"
        })
    )
    message=forms.CharField(widget=forms.Textarea(attrs={
        "class": "form-control",
        "placeholder": "Enter your message here"
        })
    )
