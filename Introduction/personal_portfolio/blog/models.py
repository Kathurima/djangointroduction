from django.db import models

# Create your models here.
class Category(models.Model):
    """docstring for Category."""
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name

class Post(models.Model):
    """docstring for Post."""
    title = models.CharField(max_length=255)
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    categories = models.ManyToManyField('Category', related_name='posts')   # Many categories can be assigned to many posts.

    def __str__(self):
        return self.title

class Comment(models.Model):
    """docstring for Comment."""
    author = models.CharField(max_length=60)
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)  #One post can have many comments

    def __str__(self):
        return self.author
    def __str__(self):
        return self.body
