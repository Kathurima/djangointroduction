from django import forms

class CommentForm(forms.Form):
    """docstring for CommentForm."""
    # forms.TextInput widget tells Django to load this field as an HTML text input element in the templates
    author = forms.CharField(max_length=60, widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Your name"
        })
    )
    # forms.TextArea widget instead, so that the field is rendered as an HTML text area element.
    body = forms.CharField(widget=forms.Textarea(attrs={
        "class": "form-control",
        "placeholder": "Leave a comment!"
        })
    )
