from django.contrib import admin
from blog.models import Post, Category, Comment

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    """docstring for PostAdmin."""
    pass

class CategoryAdmin(admin.ModelAdmin):
    """docstring for CategoryAdmin."""
    pass

class CommentAdmin(admin.ModelAdmin):
    """docstring for CommentAdmin."""
    pass


admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentAdmin)
