from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from .models import Question, Choice

# Create your views here.
class IndexView(generic.ListView):
	"""docstring for IndexView."""
	template_name = 'polls/index.html'
	context_object_name = 'latest_question_list'

	def get_queryset(self):
		"""Return the last five published questions (Excluding
		those set to be published in the future)"""

		"""Question.objects.filter(pub_date__lte=timezone.now())
		returns a queryset containing Questions whose pub_date is
		less than or equal to - that is, earlier than or equal to -
		timezone.now."""
		return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
	"""docstring for DetailView."""
	model = Question
	template_name = 'polls/detail.html'

	def get_queryset(self):
		"""Excludes any questions that aren't published yet."""
		return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
	"""docstring for ResultsView."""
	model = Question
	template_name = 'polls/results.html'

def vote(request, question_id):
	question = get_object_or_404(Question, pk = question_id)
	try:
		selected_choice = question.choice_set.get(pk = request.POST['choice'])
	except (KeyError, Choice.DoesNotExist):
		# Redisplaying the questions voting form
		return render(request, 'polls/detail.html', {
			'question': question,
			'error_message': "You didn't select a choice.",
		})
	else:
		selected_choice.votes += 1
		selected_choice.save() # Saving the selected choice

		# Always return a HttpResponseRedirect after successfully dealing with
		# POST data. This prevents data from being posted twice if the user hits back button
		return HttpResponseRedirect(reverse('polls:results', args = (question.id,)))
